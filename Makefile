# Usage:
# * make clean
# * make all
# * make upload
#
# The following need to be defined:
# ARDUINO_MAKEFILE
# MONITOR_PORT

ALTERNATE_CORE = ATTinyCore
BOARD_TAG = attinyx5
BOARD_SUB = 85
VARIANT = tinyX5
F_CPU = 8000000L
ISP_PROG = arduino

ARDUINO_LIBS = SoftwareSerial TinyWireM TinyRTCLib Time

include $(ARDUINO_MAKEFILE)
