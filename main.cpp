#include <Arduino.h>
#include <SoftwareSerial.h>

#define SERIAL_BAUD  (2700)
#define THERM_COEFF  (0.0916)
#define THERM_OFFSET (-21.365)

/**
 * ATTiny85 Pinout:
             -------------
          NC |1 PB5 VCC 8| 3.3v Power
          RX |2 PB3 PB2 7| SCL
          TX |3 PB4 PB1 6| Ain1
      Ground |4 GND PB0 5| SDA
             -------------
 */

SoftwareSerial S_Serial(3, 4); // RX=D3, Pin 2, TX=D4, Pin 3

void setup() {
    S_Serial.begin(SERIAL_BAUD);
}

void loop() {
    union {
        float _union_float;
        uint8_t _union_buf[sizeof(float)];
    };

    _union_float = THERM_COEFF * analogRead(A1) + THERM_OFFSET;

    for (uint8_t i = 0; i < sizeof(_union_buf); i++) {
        S_Serial.write(_union_buf[i]);
    }

    delay(-1); // Sleep infinite
}
